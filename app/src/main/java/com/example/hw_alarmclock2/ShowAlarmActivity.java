package com.example.hw_alarmclock2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class ShowAlarmActivity extends AppCompatActivity {

    Button mButton;
    TextView mTime1,mTime2,mTime3,mTime4;
    TextView mRepeat1,mRepeat2,mRepeat3,mRepeat4;
    CheckBox mBox1,mBox2,mBox3,mBox4;
    AlarmHelper mAlarmHelper;
    String mStrTime, mStrRepeat, mUri;
//    Map<CheckBox, TextView> mBoxMap = new HashMap<>();
//    Map<TextView,TextView> mTextMap = new HashMap<>();
    Map<Integer,AlarmInfo> mBoxMap = new HashMap<>();
    private AlarmDB alarmDB;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_alarm);

        openDB();

        mAlarmHelper = new AlarmHelper(this);
        mButton = findViewById(R.id.btnNewAlarm);
        mButton.setBackgroundColor(getColor(R.color.dark_red));
        mButton.setOnClickListener(setNewAlarm);

        mBox1 = findViewById(R.id.chkBox1);
        mBox2 = findViewById(R.id.chkBox2);
        mBox3 = findViewById(R.id.chkBox3);
        mBox4 = findViewById(R.id.chkBox4);

        mTime1 = findViewById(R.id.txtShowTime1);
        mTime2 = findViewById(R.id.txtShowTime2);
//        System.out.println("mTime1:"+mTime1.getText());
//        System.out.println("mTime2:"+mTime2.getText());
//        System.out.println(mTime1.getText().toString().equals("Time"));
        mTime3 = findViewById(R.id.txtShowTime3);
        mTime4 = findViewById(R.id.txtShowTime4);
        mRepeat1 = findViewById(R.id.txtShowRepeat1);
        mRepeat2 = findViewById(R.id.txtShowRepeat2);
        mRepeat3 = findViewById(R.id.txtShowRepeat3);
        mRepeat4 = findViewById(R.id.txtShowRepeat4);

        mBox1.setOnCheckedChangeListener(this::onCheckedChanged);
        mBox2.setOnCheckedChangeListener(this::onCheckedChanged);
        mBox3.setOnCheckedChangeListener(this::onCheckedChanged);
        mBox4.setOnCheckedChangeListener(this::onCheckedChanged);

//        mBox1.setEnabled(false);
//        mBox2.setEnabled(false);
//        mBox3.setEnabled(false);
//        mBox4.setEnabled(false);

//        mBoxMap.put(mBox1,mTime1);
//        mBoxMap.put(mBox2,mTime2);
//        mBoxMap.put(mBox3,mTime3);
//        mBoxMap.put(mBox4,mTime4);
//        mTextMap.put(mTime1,mRepeat1);
//        mTextMap.put(mTime2,mRepeat2);
//        mTextMap.put(mTime3,mRepeat3);
//        mTextMap.put(mTime4,mRepeat4);

    }

    @Override
    protected void onResume() {
        System.out.println("resume");
        super.onResume();
        loadFromDB();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    //NewAlarmBtn listener
    private View.OnClickListener setNewAlarm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent it = new Intent();
            it.setClass(ShowAlarmActivity.this, SetAlarmActivity.class);
//            startActivity(it);
            startActivityForResult(it,1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            super.onActivityResult(requestCode, resultCode, data);
            mStrTime = data.getStringExtra("TIME");
            mStrRepeat  = data.getStringExtra("REPEAT");
            mUri = data.getStringExtra("RING_URI");
            AlarmInfo alarmInfo = new AlarmInfo(mStrTime,mStrRepeat,mUri);
            if (mTime1.getText().toString().equals("Time") && mRepeat1.getText().toString().equals("Repeat")) {
                addToDB(mBox1.getId(),mStrTime,mStrRepeat,mUri);
            } else if (mTime2.getText().toString().equals("Time") && mRepeat2.getText().toString().equals("Repeat")) {
                addToDB(mBox2.getId(),mStrTime,mStrRepeat,mUri);
            } else if (mTime3.getText().toString().equals("Time") && mRepeat3.getText().toString().equals("Repeat")) {
                addToDB(mBox3.getId(),mStrTime,mStrRepeat,mUri);
            } else if (mTime4.getText().toString().equals("Time") && mRepeat4.getText().toString().equals("Repeat")) {
                addToDB(mBox4.getId(),mStrTime,mStrRepeat,mUri);
            } else {
                mAlarmHelper.closeAlarm(mBox1.getId(),mBoxMap.get(mBox1.getId()).getTime()
                        ,mBoxMap.get(mBox1.getId()).getRepeat()
                        ,mBoxMap.get(mBox1.getId()).getRing());
                addToDB(mBox1.getId(),mStrTime,mStrRepeat,mUri);
                updateCheck(mBox1.getId(), false);
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onCheckedChanged(CompoundButton c, boolean isChecked) {
        updateCheck(c.getId(),isChecked);
        if (isChecked){
            mAlarmHelper.openAlarm(c.getId(),mBoxMap.get(c.getId()).getTime()
                    ,mBoxMap.get(c.getId()).getRepeat()
                    ,mBoxMap.get(c.getId()).getRing());
//            mAlarmHelper.openAlarm(c.getId(),mStrTime,"Mon,Thu",mUri);
        }else {
            mAlarmHelper.closeAlarm(c.getId(),mBoxMap.get(c.getId()).getTime()
                    ,mBoxMap.get(c.getId()).getRepeat()
                    ,mBoxMap.get(c.getId()).getRing());
        }
    }

    private void openDB() {
        alarmDB = new AlarmDB(this);
    }

    private void closeDB() {
        alarmDB.close();
    }

    private void addToDB(int boxId,String time,String repeat,String ring) {
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("BOX",boxId);
        values.put("TIME",time);
        values.put("REPEAT",repeat);
        values.put("RINGTONE",ring);
        db.replace("MyAlarm", null, values);
    }

    private void updateCheck(int boxId, boolean checkOrNot) {
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECK_OR_NOT", checkOrNot);
        db.update("MyAlarm", values, "BOX=" + boxId, null);
    }

    @SuppressLint("NonConstantResourceId")
    private void loadFromDB() {
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        //Cursor資料指標
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery("SELECT * FROM MyAlarm", null);
        while (cursor.moveToNext()) {
            //1: BOX
            //2: TIME
            //3: REPEAT
            //4: RINGTONE
            //5: CHECK_OR_NOT
            int boxId = cursor.getInt(1);
            String time = cursor.getString(2);
            String repeat = cursor.getString(3);
            String ringtone = cursor.getString(4);

            mBoxMap.put(boxId, new AlarmInfo(time, repeat, ringtone));
            switch (boxId) {
                case R.id.chkBox1:
                    mTime1.setText(cursor.getString(2));
                    mRepeat1.setText(cursor.getString(3));
                    mBox1.setChecked(cursor.getInt(5) > 0);
                    break;
                case R.id.chkBox2:
                    mTime2.setText(cursor.getString(2));
                    mRepeat2.setText(cursor.getString(3));
                    mBox2.setChecked(cursor.getInt(5) > 0);
                    break;
                case R.id.chkBox3:
                    mTime3.setText(cursor.getString(2));
                    mRepeat3.setText(cursor.getString(3));
                    mBox3.setChecked(cursor.getInt(5) > 0);
                    break;
                case R.id.chkBox4:
                    mTime4.setText(cursor.getString(2));
                    mRepeat4.setText(cursor.getString(3));
                    mBox4.setChecked(cursor.getInt(5) > 0);
                    break;
                default:
                    break;
            }
        }
    }

}