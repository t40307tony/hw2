package com.example.hw_alarmclock2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    MyHandler mHandler;
    String[] mStrCastToEn = {"SUN", "MON", "TUE",
            "WED", "THU", "FRI", "SAT"};
    Thread mThread;
    TextView mTimeDisplay,mDateDisplay;
    int miMonth,miDay,miHour,miMinute;
    String mStrWeek,mStrCastAmPm;

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            do {
                getTime();
                Message msg = new Message();
                mHandler.sendMessage(msg);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } while (true);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler = new MyHandler();
        mTimeDisplay = findViewById(R.id.txtTime);
        mDateDisplay = findViewById(R.id.txtDate);
        Button mButton = findViewById(R.id.btnSet);
        mButton.setBackgroundColor(getColor(R.color.dark_red));

        mButton.setOnClickListener(v -> {
            Intent it = new Intent();
            it.setClass(this, ShowAlarmActivity.class);
            startActivity(it);
        });

        mThread = new Thread(runnable);
        mThread.start();
    }

    private void getTime() {
        Calendar c = Calendar.getInstance();
        miMonth = c.get(Calendar.MONTH);
        miDay = c.get(Calendar.DAY_OF_MONTH);
        mStrWeek = mStrCastToEn[c.get(Calendar.DAY_OF_WEEK) - 1];
        miHour = c.get(Calendar.HOUR);
        miMinute = c.get(Calendar.MINUTE);
        int miAmPm = c.get(Calendar.AM_PM);
        if (miAmPm == 0) {
            mStrCastAmPm = "AM";
        } else {
            mStrCastAmPm = "PM";
        }
    }

    private void updateDisplay() {
        mTimeDisplay.setText(new StringBuilder().append(pad(miHour)).append(":")
                .append((pad(miMinute))).append(" ").append(mStrCastAmPm));

        mDateDisplay.setText(new StringBuilder().append(mStrWeek)
                .append(", ").append(miMonth + 1).append("/").append(miDay));
    }

    //小於10的數字補0
    private String pad(int c) {
        if (c >= 10){
            return String.valueOf(c);
        }
        else {
            return "0" + c;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThread.interrupt();
    }

    class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            updateDisplay();
        }
    }
}