package com.example.hw_alarmclock2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AlertAlarmActivity extends AppCompatActivity {

    private TextView mTime;
    private Button mCloseBtn,mLaterBtn;
    private Uri mUri;
    String mStrRingUri,mStrTime,mStrRepeat;
    Ringtone mRingtone;
    AlarmHelper mAlarmHelper;
    int miId;
    AlarmDB mAlarmDB;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_alarm);

        System.out.println("Enter Alert!");

        mAlarmDB = new AlarmDB(this);
        mAlarmHelper = new AlarmHelper(this);

        mTime = findViewById(R.id.txtShowAlarm);
        mCloseBtn = findViewById(R.id.btnClose);
        mLaterBtn = findViewById(R.id.btnLater);

        mCloseBtn.setBackgroundColor(getColor(R.color.dark_red));
        mLaterBtn.setBackgroundColor(getColor(R.color.dark_red));

        mCloseBtn.setOnClickListener(closeOnClick);
        mLaterBtn.setOnClickListener(laterOnClick);

        Intent it = getIntent();
        mStrTime = it.getStringExtra("TIME");
        mStrRingUri = it.getStringExtra("RING_URI");
        mStrRepeat = it.getStringExtra("REPEAT");
        miId = it.getIntExtra("ID",-1);

        System.out.println("Time:"+mStrTime);
        System.out.println("Ring_Uri:"+mStrRingUri);
        mTime.setText(mStrTime);
        AlarmDB alarmDB = new AlarmDB(this);
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM MyAlarm WHERE BOX = ?", new String[]{String.valueOf(miId)});
        AlarmInfo alarmInfo = new AlarmInfo();
        while(cursor.moveToNext()) {
            alarmInfo = new AlarmInfo(cursor.getString(2), cursor.getString(3), cursor.getString(4));
            break;
        }

        mAlarmHelper.openAlarm(miId,alarmInfo.getTime()
                ,alarmInfo.getRepeat()
                ,alarmInfo.getRing());

//        startAlarmRing();
    }

    //Close Listener
    private View.OnClickListener closeOnClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            finishAndRemoveTask();
//            stopAlarmRing();
            if(mStrRepeat.equals("No Repeat")){
                updateCheck(miId,false);
            }
        }
    };

    //Later Listener
    private View.OnClickListener laterOnClick = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            finishAndRemoveTask();
//            stopAlarmRing();
            setLaterAlarm();
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void startAlarmRing() {
        mUri = Uri.parse(mStrRingUri);
        mRingtone = RingtoneManager.getRingtone(this, mUri);
        mRingtone.setLooping(false);
        if(!mRingtone.isPlaying()) {
            mRingtone.play();
        }
    }

    private void stopAlarmRing() {
        if(mRingtone.isPlaying()) {
            mRingtone.stop();
        }
    }

    private void setLaterAlarm(){
        int setHour;
        Date newTime = null;
        String[] tempTime = mStrTime.split("\\s|:");
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a", Locale.ENGLISH);
        try {
            setHour = Integer.parseInt(tempTime[0]);
            String tempTime2 = now.get(Calendar.YEAR) + "/" + (now.get(Calendar.MONTH) + 1)
                    + "/" + now.get(Calendar.DATE)
                    + " " + setHour + ":" + (Integer.parseInt(tempTime[1]) + 10)
                    + " " + tempTime[2];
            newTime = sdf.parse(tempTime2);
        } catch (ParseException ex) {
            ex.getMessage();
        }
        sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        mAlarmHelper.openAlarm(miId,sdf.format(newTime),"No Repeat",mStrRingUri);
    }

    private void updateCheck(int boxId, boolean checkOrNot) {
        SQLiteDatabase db = mAlarmDB.getWritableDatabase();
        //ContentValues 用來裝新增的資料
        ContentValues values = new ContentValues();
        values.put("CHECK_OR_NOT", checkOrNot);
        System.out.println("AlertBOX:"+boxId);
        db.update("MyAlarm", values, "BOX=" + boxId, null);
    }
}