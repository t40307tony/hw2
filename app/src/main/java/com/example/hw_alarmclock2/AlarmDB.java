package com.example.hw_alarmclock2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlarmDB extends SQLiteOpenHelper {

    private final static int DBVersion = 1;
    private final static String DBName = "MyAlarm.db";
    private final static String tableName = "MyAlarm";

    public AlarmDB(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL = "CREATE TABLE IF NOT EXISTS " + tableName + "( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," + "BOX INTEGER UNIQUE,"
                + "TIME VARCHAR(20)," + "REPEAT VARCHAR(30)," + "RINGTONE VARCHAR(30),"
                + "CHECK_OR_NOT BOOLEAN DEFAULT 'FALSE'" + ");";
        db.execSQL(SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String SQL = "DROP TABLE " + tableName;
        db.execSQL(SQL);
    }
}
