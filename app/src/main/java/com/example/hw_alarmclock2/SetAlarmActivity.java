package com.example.hw_alarmclock2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SetAlarmActivity extends AppCompatActivity {

    TextView mSetTime, mSetRepeat, mSetRing;
    Button mCancel,mSave;
    Uri mUri;
    Ringtone mRingtone;
    ArrayList<String> list = new ArrayList<>();
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    DateAdapter.ViewHolder holder;
    ListView myListView;
    View layout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);

        initDate();
//        System.out.println("list:"+list.isEmpty());

        mSetTime = findViewById(R.id.txtSetTime);
        mSetRepeat = findViewById(R.id.txtSetRepeat);
        mSetRing = findViewById(R.id.txtSetRing);
        mCancel = findViewById(R.id.btnCancel);
        mSave = findViewById(R.id.btnSave);

        mCancel.setBackgroundColor(getColor(R.color.dark_red));
        mSave.setBackgroundColor(getColor(R.color.dark_red));

        //預設
        mUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        mRingtone = RingtoneManager.getRingtone(SetAlarmActivity.this, mUri);
        String title = mRingtone.getTitle(SetAlarmActivity.this);
        mSetTime.setText("00:00 AM");
        mSetRepeat.setText("No Repeat");
        mSetRing.setText(title);

        mSetTime.setOnClickListener(setTimeOnClick);
        mSetRepeat.setOnClickListener(setRepeatOnClick);
        mSetRing.setOnClickListener(setRingOnClick);
        mCancel.setOnClickListener(setCancelOnClick);
        mSave.setOnClickListener(setSaveOnClick);



    }

    //get ringtone title
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Enter to Activity Result!");
        if (resultCode != RESULT_OK) {
        } else {
            mUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
//        System.out.println("Ring uri:" + uri);
            mRingtone = RingtoneManager.getRingtone(this, mUri);
            String title = mRingtone.getTitle(this);
            mSetRing.setText(title);
        }
    }

    // mSetTime listener
    private View.OnClickListener setTimeOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar now = Calendar.getInstance();

            TimePickerDialog timePickerDlg = new TimePickerDialog(SetAlarmActivity.this,
                    timePickerListener, now.get(Calendar.HOUR),
                    now.get(Calendar.MINUTE), false);
            timePickerDlg.show();
        }
    };

    // TimePickerDialog listener
    private TimePickerDialog.OnTimeSetListener timePickerListener
            = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String pmTime = (hourOfDay - 12) + ":" + minute + " " + "PM";
            String amTime = hourOfDay + ":" + minute + " " + "AM";
            if (hourOfDay > 12) {
                mSetTime.setText(pmTime);
            } else {
                mSetTime.setText(amTime);
            }
        }
    };

    //mSetRepeat listener
    private View.OnClickListener setRepeatOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Calendar now = Calendar.getInstance();
//            DatePickerDialog datePickerDlg = new DatePickerDialog(SetAlarmActivity.this,
//                    datePickerListener, now.get(Calendar.YEAR), now.get(Calendar.MONTH)
//                    , now.get(Calendar.DAY_OF_MONTH));
//            //設定日曆可選範圍
//            DatePicker datePicker = datePickerDlg.getDatePicker();
//            datePicker.setMinDate(System.currentTimeMillis()+ TimeUnit.DAYS.toMillis(1));
//            datePicker.setMaxDate(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7));
//            datePickerDlg.show();
            ShowDialog();
        }
    };

    // DatePickerDialog listener
//    private DatePickerDialog.OnDateSetListener datePickerListener
//            = new DatePickerDialog.OnDateSetListener() {
//        @Override
//        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//            String dayOfWeek = DateFormat.format("EE", new Date(year, month, dayOfMonth-1))
//                    .toString();
//            mSetRepeat.setText(dayOfWeek);
//        }
//    };

    //mSetRing listener
    private View.OnClickListener setRingOnClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent it = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
            it.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
            startActivityForResult(it,RESULT_OK);
//            System.out.println("Start Activity!");
        }
    };

    //Cancel listener
    private View.OnClickListener setCancelOnClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            Ringtone ringtone = RingtoneManager.getRingtone(SetAlarmActivity.this, uri);
            String title = ringtone.getTitle(SetAlarmActivity.this);
            //時間、Repeat、Ring 變回預設狀態
            mSetTime.setText("00:00 AM");
            mSetRepeat.setText("No Repeat");
            mSetRing.setText(title);
        }
    };

    //Save listener
    private View.OnClickListener setSaveOnClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("Save Click!");
            Intent it = getIntent();
            it.putExtra("TIME",mSetTime.getText());
            it.putExtra("REPEAT",mSetRepeat.getText());
            it.putExtra("RING_URI", mUri.toString());
            System.out.println("Ring_Uri:"+mUri);
            it.setClass(SetAlarmActivity.this,ShowAlarmActivity.class);
            setResult(0,it);
            finish();
        }
    };

    private void initDate() {
        list.add("Monday");
        list.add("Tuesday");
        list.add("Wednesday");
        list.add("Thursday");
        list.add("Friday");
        list.add("Saturday");
        list.add("Sunday");
    }

    //Show ListView
    public void ShowDialog() {
        //取得xml裡定義的ListView
        LayoutInflater inflater = (LayoutInflater) SetAlarmActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.date_list_view, null);
        myListView = (ListView) layout.findViewById(R.id.listView);
        DateAdapter adapter = new DateAdapter(SetAlarmActivity.this,list);
        myListView.setAdapter(adapter);
//        myListView.setOnItemClickListener(dateViewClickListener);
//        builder = new AlertDialog.Builder(SetAlarmActivity.this);
//        builder.setView(layout);
//        alertDialog = builder.create();
//        alertDialog.show();
        new AlertDialog.Builder(SetAlarmActivity.this).setView(layout)
                .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSetRepeat.setText("");
                        for(Integer a: adapter.getIsSelected().keySet()){
                            if(adapter.getIsSelected().get(a)){
                                String day = adapter.getItem(a).toString();
                                day = day.substring(0,3);
                                mSetRepeat.setText(mSetRepeat.getText()+","+day);
                                System.out.println("key is " + adapter.getItem(a));
                            }
                        }
                        if(mSetRepeat.getText().equals("")) {
                            mSetRepeat.setText("No Repeat");
                        }
                        else {
                            String result = mSetRepeat.getText().toString();
                            mSetRepeat.setText(result.substring(1));
                        }

                    }
                }).setNegativeButton("CANCEL",null).create().show();

    }

//    //ListView listener
//    private AdapterView.OnItemClickListener dateViewClickListener = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            System.out.println("Enter to Listener!");
//            DateAdapter.ViewHolder holder = (DateAdapter.ViewHolder) view.getTag();
//            //把CheckBox的狀態改為當前狀態的相反
//            holder.chk.toggle();
//            DateAdapter.getIsSelected().put(position,holder.chk.isChecked());
//                    for (Map.Entry<Integer, Boolean> e :  DateAdapter.isSelected.entrySet()) {
//            System.out.println(e.getKey() + ":" + e.getValue());
//            }
//        }
//    };
}