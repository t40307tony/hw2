package com.example.hw_alarmclock2;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateAdapter extends BaseAdapter {
    private List<String> mList;
    private Context mContext;
    public static HashMap<Integer,Boolean> isSelected;
    private static LayoutInflater inflater = null;

    public DateAdapter(Context mContext,List<String> list) {
        this.mList = list;
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isSelected = new HashMap<>();
        initDate();
    }

    //chk初始設定為false
    private void initDate() {
        for(int i = 0; i < mList.size(); i++){
            getIsSelected().put(i,false);
        }
    }

    public HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

//    public static void setIsSelected(HashMap<Integer, Boolean> isSelected) {
//        DateAdapter.isSelected = isSelected;
//    }

    //取得ListView長度(item個數)
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //設定每一行ListView
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        //檢查有沒有可以重複使用的view
        if(convertView == null){
            convertView = inflater.inflate(R.layout.date_item,null);
            holder = new ViewHolder();
            holder.txt = convertView.findViewById(R.id.txtWeek);
            holder.chk = convertView.findViewById(R.id.chkWeek);
            //將holder綁至convertView中，使得可以重複使用此view
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txt.setTextColor(Color.BLACK);
        holder.txt.setText(mList.get(position));
        holder.chk.setChecked(getIsSelected().get(position));
        holder.chk.setId(position);
        holder.chk.setOnCheckedChangeListener(this::onCheckedChanged);

//        for (Map.Entry<Integer, Boolean> e : isSelected.entrySet()) {
//            System.out.println(e.getKey() + ":" + e.getValue());
//        }

        return convertView;
    }

    public void onCheckedChanged(CompoundButton c, boolean isChecked) {
          if(isChecked){
                getIsSelected().put(c.getId(),true);
          }else{
              getIsSelected().put(c.getId(),false);
          }
    }

    public static class ViewHolder {
        TextView txt;
        CheckBox chk;
    }
}