package com.example.hw_alarmclock2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class AlarmHelper {

    private Context mContext;
    private AlarmManager mAlarmManager;

    public AlarmHelper(Context c) {
        this.mContext = c;
        mAlarmManager = (AlarmManager) c
                .getSystemService(Context.ALARM_SERVICE);
    }

    public void openAlarm(int id,String time,String repeat,String ring_uri) {
        System.out.println("OpenAlarm!"+time);
        Intent it = new Intent();
        it.putExtra("ID",id);
        it.putExtra("TIME",time);
        it.putExtra("RING_URI",ring_uri);
        it.putExtra("REPEAT",repeat);
        it.setClass(mContext,AlertAlarmActivity.class);
        PendingIntent pi = PendingIntent.getActivity(mContext,id,it,PendingIntent.FLAG_CANCEL_CURRENT);

        if(repeat != null){
            if(repeat.equals("No Repeat")){
                System.out.println("Enter no Repeat!");
                Date newDate = null;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a", Locale.ENGLISH);
                Calendar now = Calendar.getInstance();
                String totalTime = now.get(Calendar.YEAR) + "/"
                        + (now.get(Calendar.MONTH) + 1) + "/"
                        + now.get(Calendar.DATE) + " " + time;
                try{
                    newDate  = sdf.parse(totalTime);
                }catch (ParseException ex){
                    ex.printStackTrace();
                }
                long newTime = newDate.getTime();
                mAlarmManager.set(AlarmManager.RTC_WAKEUP,newTime,pi);
            }else{
                String[] tmpRepeat = repeat.split("\\s|,");
                Calendar now = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a", Locale.ENGLISH);
                int nowWeek = (now.get(Calendar.DAY_OF_WEEK)+5)%7+1;

                int closeDay = -1;

                String clockTime = now.get(Calendar.YEAR) + "/"
                        + (now.get(Calendar.MONTH) + 1) + "/"
                        + (now.get(Calendar.DATE))+ " " + time;
                Date clockDate = null;
                try{
                    clockDate  = sdf.parse(clockTime);
                }catch (ParseException ex){
                    ex.printStackTrace();
                }

                boolean isUpToDate = !clockDate.after(now.getTime());
                System.out.println(now.getTime());

                for(String castRepeat : tmpRepeat){
                    int castInt = parseWeekToInt(castRepeat);
                    int dayDiff = castInt - nowWeek;
                    if(isUpToDate && dayDiff ==0)
                        dayDiff = 7;
                    if(closeDay==-1) {
                        closeDay = dayDiff>=0? dayDiff : 7+dayDiff;
                    }
                    else {
                        if(dayDiff>=0) {
                            closeDay = (closeDay > dayDiff) ? castInt - nowWeek : closeDay;
                        }
                        else{
                            closeDay = (7+dayDiff<closeDay) ? 7+dayDiff : closeDay;
                        }
                    }
                }

                System.out.println("close day " + closeDay);
                String totalTime = now.get(Calendar.YEAR) + "/"
                        + (now.get(Calendar.MONTH) + 1) + "/"
                        + (now.get(Calendar.DATE)+closeDay)+ " " + time;
                Date newDate = null;
                try{
                    newDate  = sdf.parse(totalTime);
                }catch (ParseException ex){
                    ex.printStackTrace();
                }
                long newTime = newDate.getTime();
                mAlarmManager.set(AlarmManager.RTC_WAKEUP,newTime,pi);
            }
        }
    }

    public void closeAlarm(int id,String time,String repeat,String ring_uri) {
        System.out.println("CloseAlarm!"+time);
        Intent it = new Intent();
        it.putExtra("ID",id);
        it.putExtra("TIME",time);
        it.putExtra("REPEAT",repeat);
        it.putExtra("RING_URI",ring_uri);
        it.setClass(mContext,AlertAlarmActivity.class);
        PendingIntent pi = PendingIntent.getActivity(mContext, id, it, 0);
        mAlarmManager.cancel(pi);
    }

    //轉換week name成int
    public static int parseWeekToInt(String str){
        System.out.println("Enter to parse!");
        if("Mon".equals(str)){
            return 1;
        }else if ("Tue".equals(str)){
            return 2;
        }else if("Wed".equals(str)){
            return 3;
        }else if("Thu".equals(str)){
            return 4;
        }else if("Fri".equals(str)){
            return 5;
        }else if("Sat".equals(str)){
            return 6;
        }else if("Sun".equals(str)){
            return 7;
        }else{
            return -1;
        }
    }
}

