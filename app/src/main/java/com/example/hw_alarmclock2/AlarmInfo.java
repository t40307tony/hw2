package com.example.hw_alarmclock2;

public class AlarmInfo {
    String time;
    String repeat;
    String ring;

    public AlarmInfo(String time, String repeat, String ring) {
        this.time = time;
        this.repeat = repeat;
        this.ring = ring;
    }

    public AlarmInfo() {

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getRing() {
        return ring;
    }

    public void setRing(String ring) {
        this.ring = ring;
    }
}
